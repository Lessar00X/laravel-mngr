@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Список пользователей</div>

                <div class="card-body">


                    @forelse ($users as $user)

                        <div>
                            <a href="/lk/users/info/{{$user->id}}/">{{$user->name}}</a>
                        </div>

                    @empty

                        Нет пользователей

                    @endforelse
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
