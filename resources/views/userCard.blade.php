@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Информация о пользователе #{{$user->id}}</div>

                <div class="card-body">

                    <ul>
                        <li> #ID : {{$user->id}};</li>
                        <li> #NAME : {{$user->name}};</li>
                    </ul>


                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Комментарии менеджера</div>

                <div class="card-body">
                    @forelse ($user->comments as $comment)

                    <div class="card"  style="margin-bottom: 5px;">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <ul>
                                        <li><b>Дата:</b> {{$comment->created_at}}</li>
                                        <li><b>Менеджер:</b> {{$comment->manager->name}}</li>
                                        <li><b>Комментарий:</b> {{$comment->comment}}</li>
                                        @if ($comment->file)
                                           <img src="{{$comment->file->url}}" style="width: 100%">
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    @empty

                    Нет комментариев

                    @endforelse
                </div>
            </div>
        </div>
    </div>

<hr>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Добавить комментарий</div>
                    <div class="card-body">
                        <div class="container">
                            <div class="row">
                                <div class="col-12">
                                    @if ($errors->any())
                                    <div class="alert alert-danger alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                            <li>
                                                {{ $error }}
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @endif
                                    <form action="{{ route('user.comment.add') }}" method="POST" role="form" enctype="multipart/form-data">
                                        @csrf
                                        <input id="user_id" type="hidden" name="user_id" value="{{$user->id}}">
                                        <div class="form-group row">
                                            <label for="name" class="col-md-4 col-form-label text-md-right">Комментарий</label>
                                            <div class="col-md-6">
                                                <textarea class="form-control" id="comment" name="comment" placeholder="Введите комментарий"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="comment_image" class="col-md-4 col-form-label text-md-right">Прикрепить картинку</label>
                                            <div class="col-md-6">
                                                <input id="comment_image" type="file" class="form-control" name="comment_image">
                                            </div>
                                        </div>
                                        <div class="form-group row mb-0 mt-5">
                                            <div class="col-md-8 offset-md-4">
                                                <button type="submit" class="btn btn-primary">Добавить</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>







</div>
@endsection
