<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::group(['middleware' => ['auth', 'manager']], function () {
    Route::get('/lk/users/all', 'ManagerController@AllUsers')->name('users.all');
    Route::get('/lk/users/info/{id}', 'ManagerController@getUser');
    Route::post('/user/comments/add', 'ManagerController@addComment')->name('user.comment.add');
});


