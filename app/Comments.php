<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    protected $fillable = [
        'user_id', 'manager_id', 'comment', 'file_id'
    ];

    public function manager() {
        return $this->belongsTo('\App\User');
    }

    public function file() {
        return $this->belongsTo('\App\File');
    }
}
