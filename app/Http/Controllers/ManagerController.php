<?php

namespace App\Http\Controllers;

use App\Jobs\ProcessSendingComments;
use App\User;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class ManagerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function AllUsers() {
        $allUsers = \App\User::where('role', 'user')->get();
        return view('allUsers', ['users' => $allUsers]);
    }

    public function getUser( Request $request, $id ) {
        //->where('role', 'user')
        $user = User::where('id', $id)->with('comments')->get()->first();

        if ($user) {
            return view('userCard', ['user' => $user]);
        } else {
            abort(404);
        }
    }


    public function addComment(Request $request) {
        $request->validate([
            'comment'           =>  'required|max:500',
            'comment_image'     =>  'required|image|mimes:jpeg,png,jpg,gif|max:2048'
        ]);

        $user = User::findOrFail($request->user_id);

        $newComment = [
            'user_id'     => $user->id,
            'manager_id'  => auth()->user()->id,
            'comment'     => $request->comment
        ];


        if ($request->has('comment_image')) {
            $image = $request->file('comment_image');
            $name = time();
            $folder = '/uploads/images/';
            $filePath = $folder.$name.'.'.$image->getClientOriginalExtension();

            $name = !is_null($name) ? $name : Str::random(25);

            $file = $image->storeAs($folder, $name.'.'.$image->getClientOriginalExtension(), 'public');

            if ($file) {
                $newFile = \App\File::create([
                    'name' => $name,
                    'url'  => $filePath
                ]);

                $newComment['file_id'] = $newFile->id;
            }
        }

        $comment = \App\Comments::create($newComment);

        ProcessSendingComments::dispatch($comment);

        return redirect()->back()->with(['status' => 'Комментарий успешно добавлен']);
    }
}
